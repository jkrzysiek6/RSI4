﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Kontrakt
{
    //[ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    //public class PhotoService : IPhotoService, IDuplexPhotosListService
    public class PhotoService : IPhotoService
    {
        private string path = Path.Combine(Environment.CurrentDirectory, "photos");


        public string[] DownloadFilesList()
        {
            Console.WriteLine("List updated.");
            return GetFileNames();
        }

        private string[] GetFileNames()
        {
            return Directory.GetFiles(path, "*.jpg")
                         .Select(filePath => Path.GetFileName(filePath))
                         .ToArray();
        }

        public long GetFileSize(string fileName)
        {
            string filePath = Path.Combine(path, fileName);

            FileStream fileStream = File.OpenRead(filePath);
            Console.WriteLine("Size of " + fileName + ": " + fileStream.Length);
            return fileStream.Length;
        }

        public Stream DownloadPhoto(string photoName)
        {
            string filePath = Path.Combine(path, photoName);
            try
            {
                FileStream imageFile = File.OpenRead(filePath);
                Console.WriteLine("Downloaded " + photoName);
                return imageFile;
            }
            catch (IOException ex)
            {
                Console.WriteLine(String.Format("Wyjatek otwarcia pliku {0}", filePath));
                Console.WriteLine("Wyjatek: ");
                Console.WriteLine(ex.ToString());
                throw ex;
            }
        }

        public bool UploadPhoto(Stream inStream)
        {
            try
            {
                int photoNr = GetFileNames().Length;
                string photoName = "photo" + photoNr + ".jpg";
                string filePath = Path.Combine(path, photoName);
                SaveFile(inStream, filePath);
                Console.WriteLine("Uploaded " + photoName);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
                return false;
            }
        }

        private void SaveFile(Stream inStream, string filePath)
        {
            FileStream outStream = File.Open(filePath, FileMode.Create, FileAccess.Write);
            CopyStream(inStream, outStream);
            outStream.Close();
            inStream.Close();
        }

        private void CopyStream(Stream inStream, Stream outStream)
        {
            const int bufferLength = 4096;
            byte[] buffer = new byte[bufferLength];
            int count = 0;

            while ((count = inStream.Read(buffer, 0, bufferLength)) > 0)
            {
                outStream.Write(buffer, 0, count);
            }
        }


        //IDuplexPhotosListCallback callback = null;
        //public PhotoService()
        //{
        //    callback = OperationContext.Current.
        //    GetCallbackChannel<IDuplexPhotosListCallback>();
        //}

        //public void DownloadList()
        //{
        //    callback.RefreshList("super");
        //}
    }
}
