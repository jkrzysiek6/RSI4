﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Kontrakt
{
    //[ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(IDuplexPhotosListCallback))]
    //public interface IDuplexPhotosListService
    //{
    //    [OperationContract(IsOneWay = true)]
    //    void DownloadList();
    //}

    //public interface IDuplexPhotosListCallback
    //{
    //    [OperationContract(IsOneWay = true)]
    //    void RefreshList(string result);
    //}


    [ServiceContract]
    public interface IPhotoService
    {
        [OperationContract]
        Stream DownloadPhoto(string photoName);

        [OperationContract]
        Boolean UploadPhoto(Stream stream);

        [OperationContract]
        string[] DownloadFilesList();

        [OperationContract]
        long GetFileSize(string fileName);
    }
}
