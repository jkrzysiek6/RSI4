﻿using Kontrakt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace Host
{
    class Program
    {
        static void Main(string[] args)
        {
            Uri baseAddress = new Uri("http://localhost:10001/ ");
            ServiceHost host = new ServiceHost(typeof(PhotoService), baseAddress);
            //ServiceEndpoint endpoint = host.AddServiceEndpoint(typeof(IDuplexPhotosListService), new WSDualHttpBinding(), "IDuplexPhotosListService");


            BasicHttpBinding b = new BasicHttpBinding();
            b.TransferMode = TransferMode.Streamed;
            b.MaxReceivedMessageSize = 607108864;
            b.MaxBufferSize = 607108864;
            ServiceEndpoint endpoint2 = host.AddServiceEndpoint(typeof(IPhotoService), b, "IPhotoService");


            ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
            smb.HttpGetEnabled = true;
            host.Description.Behaviors.Add(smb);
            try
            {
                host.Open();
                Console.WriteLine("Serwis 1 jest uruchomiony.");
                Console.WriteLine("Nacisnij <ENTER> aby zakonczyc.");
                Console.WriteLine();
                Console.ReadLine();
                host.Close();
            }
            catch (CommunicationException ce)
            {
                Console.WriteLine("Wystapil wyjatek: {0}", ce.Message);
                host.Abort();
            }
        }
    }
}

