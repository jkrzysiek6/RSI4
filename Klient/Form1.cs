﻿using Klient.PhotoServiceRef;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Klient
{
    public partial class Form1 : Form
    {
        private static PhotoServiceClient client = new PhotoServiceClient("BasicHttpBinding_IPhotoService");
        //private static DuplexPhotosListServiceClient duplexPhotosListServiceClient;

        public Form1()
        {
            InitializeComponent();
            UpdateFilesList();
        }


        private void btnList_Click(object sender, EventArgs e)
        {
            UpdateFilesList();
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            UploadFile();
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile();
        }

        private void listBox_DoubleClick(object sender, EventArgs e)
        {
            DownloadFile();
        }

        private void DownloadFile()
        {
            if (listBox.SelectedItem != null)
            {
                string photoName = listBox.SelectedItem.ToString();
                string path = Path.Combine(Environment.CurrentDirectory, "photos", photoName);
                Stream downloadedPhoto = client.DownloadPhoto(photoName);
                SaveFile(downloadedPhoto, path, client.GetFileSize(photoName));
                pictureBox.Image = new Bitmap(path);
            }
        }

        private void UploadFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Stream stream = openFileDialog.OpenFile();
                client.UploadPhoto(stream);
            }
            UpdateFilesList();
        }

        private void UpdateFilesList()
        {
            string[] fileNames = client.DownloadFilesList();
            listBox.Items.Clear();
            foreach (string name in fileNames)
            {
                listBox.Items.Add(name);
            }
        }

        private void SaveFile(Stream inStream, string filePath, long fileSize)
        {
            FileStream outStream = File.Open(filePath, FileMode.Create, FileAccess.Write);
            CopyStream(inStream, outStream, fileSize);
            outStream.Close();
            inStream.Close();
        }

        private void CopyStream(Stream inStream, Stream outStream, long fileSize)
        {
            const int bufferLength = 4096;
            byte[] buffer = new byte[bufferLength];
            int count = 0;
            progressBar.Value = 0;

            while ((count = inStream.Read(buffer, 0, bufferLength)) > 0)
            {
                outStream.Write(buffer, 0, count);
                progressBar.Value = Convert.ToInt32(outStream.Position / fileSize * 100);
            }
            Console.WriteLine("Photo saved");
        }
    }
    //public class CallbackHandler : IDuplexPhotosListCallback
    //{
    //}

}
